const Images = {
  momo: require("./momo.png"),
  vnpay: require("./vnpay.png"),
  cuff: require("./Cuff.png"),
  rope: require("./rope.png"),
  accessoryWoment: require("./accessoryWoment.png"),
  ropeWoment: require("./ropeWoment.png"),
  buon: require("./buon.png"),
  sliderHeader: require("./sliderHeader.png"),
  wristSize: require("./wristSize.png"),
};

export default Images;
