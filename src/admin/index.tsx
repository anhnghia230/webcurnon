import React, { useState } from "react";
import HandleAdmin from "./handleAdmin/handleAdmin";
import Login from "./login";

const Admin: React.FC = (props: any) => {
  const { admin } = props;
  const [account, setAccount] = useState("");
  const [password, setPassWord] = useState("");
  const [login, setLogin] = useState(false);

  const handleAccount = (e: any) => {
    setAccount(e.target.value);
  };
  const handlePassWord = (e: any) => {
    setPassWord(e.target.value);
  };

  const handleLogin = () => {
    for (let i = 0; i < admin.length; i++) {
      if (account !== "" && password !== "") {
        if (admin[i].admin === account && admin[i].passWord === password) {
          alert("login thành công");
          return setLogin(true);
        } else {
          alert("login thất bại");
          return setLogin(false);
        }
      }
    }
  };
  return login ? (
    <HandleAdmin />
  ) : (
    <Login
      handleLogin={handleLogin}
      handlePassWord={handlePassWord}
      handleAccount={handleAccount}
    />
  );
};

export default Admin;
