import Link from "next/link";
import React, { useState } from "react";

import { BsXLg, BsArrowRightShort } from "react-icons/bs";
import style from "../../styles/Admin.module.css";

const NavAdmin: React.FC = () => {
  const [hideNavAdmin, setHideNavAdmin] = useState<boolean>(false);
  return (
    <div>
      {hideNavAdmin ? (
        <div
          className={`w-[400px] h-full   ${hideNavAdmin ? style.openNav : ""}`}
        >
          <div className="flex justify-between items-center h-10 border-b border-[#d1d5db]">
            <h3 className="text-lg font-bold uppercase italic m-2">
              HandleProduct
            </h3>
            <BsXLg
              className="text-xl cursor-pointer m-2 "
              onClick={() => setHideNavAdmin(false)}
            />
          </div>
          <div className="ml-5">
            <div className="p-2 cursor-pointer hover:bg-slate-100 ">
              <Link href="/Admin/informationProduct" className="w-full">
                Information Product
              </Link>
            </div>
            <div className="p-2 cursor-pointer hover:bg-slate-100 ">
              <Link href="/Admin/addProduct" className="w-full">
                Add Product
              </Link>
            </div>
            <div className="p-2 cursor-pointer hover:bg-slate-100 ">
              <Link href="/Admin/collectStatisticalData" className="w-full">
                Collect statistical data
              </Link>
            </div>
            <div className="p-2 cursor-pointer hover:bg-slate-100 ">
              <Link href="/Admin/orderInformation" className="w-full">
                Order Information
              </Link>
            </div>
          </div>
        </div>
      ) : (
        <div className="border border-[#d5d4d3] w-10 p-1">
          <BsArrowRightShort
            className="text-3xl cursor-pointer"
            onClick={() => setHideNavAdmin(true)}
          />
        </div>
      )}
    </div>
  );
};

export default NavAdmin;
