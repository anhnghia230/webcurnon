import React from "react";
import HeaderAdmin from "./headerAdmin";
import NavAdmin from "./navAdmin";

interface LayoutAdmin {
  children: React.ReactNode;
}

const LayoutAdmin: React.FC<LayoutAdmin> = ({ children }) => {
  return (
    <div className="w-full h-[720px] ">
      <HeaderAdmin />
      <div className="flex h-full">
        <NavAdmin />
        {children}
      </div>
    </div>
  );
};

export default LayoutAdmin;
