import React from "react";

import Logo from "../../../public/image/logo";
import Image from "next/image";
import Link from "next/link";

import { AiFillCaretDown } from "react-icons/ai";
import styles from "../../styles/Admin.module.css";
import Images from "../../../public/image/image";

const HeaderAdmin = () => {
  return (
    <div className="w-full h-20 border-b border-[#d1d5db] flex justify-between  items-center ">
      <Link href="/" className="mx-10">
        <Logo />
      </Link>
      <div className="flex justify-center items-center mx-10 relative group cursor-pointer">
        <Image src={Images.cuff} alt="account" width={30} height={30} />
        <AiFillCaretDown className="mx-2 text-base" />
        <div
          className={`absolute top-10 right-0 w-40 bg-[#d1d5db] rounded-md hidden p-2  group-hover:block ${styles.account}`}
        >
          <p className="text-base p-2 hover:bg-slate-200">Admin</p>
          <div className="text-base p-2 hover:bg-slate-200 ">
            <a href="adminPage">LogOut</a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HeaderAdmin;
