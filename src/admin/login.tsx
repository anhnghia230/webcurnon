import React from "react";

const Login: React.FC = (props) => {
  const { handleAccount, handlePassWord, handleLogin }: any = props;
  return (
    <div className=" w-full h-full ">
      <div className="flex flex-col justify-center items-center mt-40">
        <label className="text-2xl uppercase font-bold">Login</label>
        <input
          type="text"
          placeholder="Admin"
          onChange={handleAccount}
          className="outline-none border border-[#dbdcdd] px-4 py-2 w-[310px] rounded-[4px] text-base my-5"
        />
        <input
          type="password"
          placeholder="Password"
          onChange={handlePassWord}
          className="outline-none border border-[#dbdcdd] px-4 py-2 w-[310px] rounded-[4px] text-base mb-5"
        />
        <button
          className="px-8 py-2 mt-5  border border-[#dbdcdd] uppercase font-bold text-sm rounded-[4px]"
          onClick={() => handleLogin()}
        >
          Login
        </button>
      </div>
    </div>
  );
};

export default Login;
