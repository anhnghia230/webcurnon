import React, { useEffect } from "react";

import CollectAdmin from "../../chart/collectAdmin";
import LayoutAdmin from "../componentAdmin/layoutAdmin";

const HandleAdmin: React.FC = () => {
  useEffect(() => CollectAdmin, []);

  return (
    <LayoutAdmin>
      <div className="w-full h-full  ">
        <p className="m-10">Chào mừng bạn đến với trang quản lý sản phẩm</p>

        <div className="w-full flex justify-center">
          <div id="collect-Admin"></div>
        </div>
      </div>
    </LayoutAdmin>
  );
};

export default HandleAdmin;
