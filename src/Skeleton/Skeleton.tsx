import React from "react";

interface className {
  className: String;
}

const Skeleton: React.FC<className> = ({ className }) => {
  return <div className={`animate-pulse bg-[#ccc] ${className}`}></div>;
};

export default Skeleton;
