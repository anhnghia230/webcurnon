import React from "react";
import * as d3 from "d3";

interface Data {
  id: number;
  total: number;
  date: string;
}

const data: Data[] = [
  {
    id: 1,
    total: 100,
    date: "1 / 2023",
  },
  {
    id: 2,
    total: 50,
    date: "2 / 2023",
  },
  {
    id: 3,
    total: 200,
    date: "3 / 2023",
  },
  {
    id: 4,
    total: 300,
    date: "4 / 2023",
  },
  {
    id: 5,
    total: 40,
    date: "5 / 2023",
  },
];

const CollectData = () => {
  const margin = { top: 30, right: 30, bottom: 60, left: 60 },
    width = 900 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

  const svg = d3
    .select("#collect-chart")
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  const maxTotal = d3.max(data, function (d: Data) {
    return d.total;
  });
  const fill = d3
    .scaleLinear()
    .domain([0, data.length])
    .range(["#fdd59c", "#fb6926"]);

  const xScale = d3
    .scaleBand()
    .domain(data.map((item) => item.date))
    .range([0, width])
    .padding(0.4);
  svg
    .append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(xScale))
    .selectAll("g")
    .select("text")
    .attr("transform", "translate(0,10)");

  const yScale = d3.scaleLinear().domain([0, maxTotal]).range([height, 0]);
  svg
    .append("g")
    .call(d3.axisLeft(yScale).ticks(7).tickSize(-width))
    .selectAll("path")
    .style("opacity", 0);

  const tooltip = d3
    .selectAll("body")
    .append("div")
    .style("position", "absolute")
    .style("border-radius", "10px")
    .style("padding", "5px 10px");

  svg.selectAll("g").selectAll("path").style("opacity", 0);
  svg.selectAll("g").selectAll("line").style("opacity", 0.2);
  svg.selectAll("g").selectAll(".tick").selectAll("text").style("opacity", 1);

  //colum

  const colum = svg
    .selectAll("collect")
    .data(data)
    .enter()
    .append("rect")
    .attr("x", function (d: Data) {
      return xScale(d.date);
    })
    .attr("width", xScale.bandwidth())
    .attr("y", function (d: Data) {
      return height;
    })
    .attr("height", function (d: Data) {
      return 0;
    })
    .attr("fill", function (d: Data, i: number) {
      return fill(i);
    })
    .attr("stroke", "#ccc")
    .on("mouseover", function (d: any, i: Data) {
      tooltip
        .transition()
        .style("opacity", 1)
        .style("border", "1px solid black")
        .style("background-color", "#ccc");
      tooltip
        .html(`<span> tháng ${i.date} : ${i.total}</span>`)
        .style("left", d.pageX + "px")
        .style("top", d.pageY + "px");
      d3.select(this).style("opacity", 0.5).style("cursor", "pointer");
    })
    .on("mouseout", function (d: any, i: Data) {
      tooltip.transition().style("opacity", 0);
      d3.select(this).style("opacity", 1);
    });

  colum
    .transition()
    .attr("y", function (d: Data) {
      return yScale(d.total);
    })
    .attr("height", function (d: Data) {
      return height - yScale(d.total);
    })
    .delay(300)
    .duration(2000);

  // line

  const line = svg
    .append("path")
    .datum(data)
    .attr("fill", "none")
    .attr("stroke", function (d: Data, i: number) {
      return fill(i);
    })
    .attr("stroke-width", 2)
    .attr(
      "d",
      d3
        .line()
        .x(function (d: Data) {
          return xScale(d.date);
        })
        .y(function (d: Data) {
          return yScale(d.total);
        })
    )
    .attr("transform", "translate(42,-40)");

  const length = line.node()?.getTotalLength();
  line
    .attr("stroke-dasharray", length + " " + length)
    .attr("stroke-dashoffset", length)
    .transition()
    .ease(d3.easeLinear)
    .attr("stroke-dashoffset", 0)
    .delay(300)
    .duration(2000);

  //circle

  const circle = svg
    .append("circle")
    .data(data)
    .attr("cx", function (d: Data) {
      return xScale(data[data.length - 1]?.date);
    })
    .attr("cy", function (d: Data) {
      return yScale(data[data.length - 1]?.total);
    })
    .attr("r", 10)
    .attr("fill", "transparent")
    .attr("transform", "translate(44,-40)");

  circle.transition().attr("fill", "#fb6926").delay(300).duration(3000);
};

export default CollectData;
