/* eslint-disable @next/next/no-img-element */
import React, { useState, useContext } from "react";

import styles from "../../../styles/Home.module.css";
import {
  BsX,
  BsFillPlusCircleFill,
  BsFillDashCircleFill,
} from "react-icons/bs";
import { TextValues } from "@/component/layout";
import { Data } from "@/interface/interface";

const Order = () => {
  const [fixOrder, setFixOrder] = useState<boolean>(false);
  const {
    productItemCart,
    setProductItemCart,
    dashProduct,
    deleteCart,
    totalPrice,
  }: any = useContext(TextValues);

  const plusProduct = (productItem: Data) => {
    const existProduct = productItemCart.find(
      (productItemCart: Data) => productItemCart._id === productItem._id
    );
    if (existProduct) {
      setProductItemCart(
        productItemCart.map((productItemCart: Data) =>
          productItemCart._id === productItem._id
            ? { ...existProduct, count: existProduct.count + 1 }
            : productItemCart
        )
      );
    } else {
      setProductItemCart([...productItemCart, { ...existProduct, count: 1 }]);
    }
  };

  const installment = Math.round(totalPrice / 3);

  return (
    <div className="py-16 px-5 bg-[#f1f0ee]">
      <div className="w-[670px] px-[60px]  ">
        <div className="flex justify-between items-center pt-4 pb-7 border-b border-[#d6d6d4]">
          <p>Đơn hàng</p>
          {fixOrder ? (
            <span
              className="underline italic opacity-60 cursor-pointer"
              onClick={() => setFixOrder(!fixOrder)}
            >
              hủy
            </span>
          ) : (
            <span
              className="underline italic opacity-60 cursor-pointer"
              onClick={() => setFixOrder(!fixOrder)}
            >
              Sửa
            </span>
          )}
        </div>
        {productItemCart.map((item: Data) => (
          <div
            className="mt-8 pb-8 h-24 flex justify-between text-sm  opacity-70 border-b border-[#d6d6d4] "
            key={item?._id}
          >
            <div className="flex justify-center items-center  ">
              {fixOrder ? (
                <BsX
                  className={`text-2xl mx-2 cursor-pointer ${styles.close}`}
                  onClick={() => deleteCart(item)}
                />
              ) : (
                ""
              )}
              <img src={item?.img} width={74} height={74} alt="product-img" />
              <div className="pl-5">
                <p className="uppercase">{item?.name}</p>
                <p className="py-1 uppercase">{item?.size}</p>
                {fixOrder ? (
                  <div className={`flex ${styles.oderQty}`}>
                    <BsFillDashCircleFill
                      className="text-lg cursor-pointer"
                      onClick={() => dashProduct(item)}
                    />
                    <label className="mx-3 "> Qty:{item?.count}</label>
                    <BsFillPlusCircleFill
                      className="text-lg cursor-pointer "
                      onClick={() => plusProduct(item)}
                    />
                  </div>
                ) : (
                  <label> Qty:{item?.count}</label>
                )}
              </div>
            </div>
            <div>
              <p className="pb-2">
                {item?.price?.toLocaleString("vi", {
                  style: "currency",
                  currency: "VND",
                })}
              </p>
              <p className="line-through opacity-50">
                {item?.priceSell?.toLocaleString("vi", {
                  style: "currency",
                  currency: "VND",
                })}
              </p>
            </div>
          </div>
        ))}
        <div className="flex py-7 border-b border-[#d6d6d4]">
          <input
            type="text"
            placeholder="Nhập mã khuyến mãi..."
            className="border-[1px] w-full h-[50px] p-2  text-base outline-none border-[rgba(85_108_147_.15)] bg-[#f8f7f4] text-center"
          />
          <button className="text-sm font-bold uppercase border-[1px] border-[rgba(85_108_147_.15)] px-4  w-[180px] h-[50px] bg-[#ecebea]">
            Áp dụng
          </button>
        </div>
        <div className="py-8 border-b border-[#d6d6d4]">
          <div className="flex justify-between pb-3">
            <p>Thành tiền </p>
            <p className="font-bold">
              {totalPrice?.toLocaleString("vi", {
                style: "currency",
                currency: "VND",
              })}
            </p>
          </div>
          <div className="flex justify-between pb-3">
            <p>Mã giảm giá </p>
            <p className="text-red-500 font-bold">0 ₫</p>
          </div>
          <div className="flex justify-between pb-3">
            <p>Phí ship </p>
            <p className="font-bold">0 ₫</p>
          </div>
        </div>
        <div className="py-8">
          <div className="flex justify-between pb-3">
            <p className="uppercase">Tổng: </p>
            <p className="font-bold text-xl">
              {totalPrice?.toLocaleString("vi", {
                style: "currency",
                currency: "VND",
              })}
            </p>
          </div>
          <div className="flex justify-between opacity-50">
            <p>{`(Đã bao gồm VAT)`}</p>
            <p>
              hoặc
              <span className="mx-1">
                {installment?.toLocaleString("vi", {
                  style: "currency",
                  currency: "VND",
                })}
              </span>
              x 3 kỳ
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Order;
