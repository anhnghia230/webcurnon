import Link from "next/link";
import React, { useContext, useState } from "react";

import Logo from "../../../../public/image/logo";
import { BsArrowRight } from "react-icons/bs";
import { Custommer } from "@/pages/paypage";
import { TextValues } from "@/component/layout";
import request from "@/axios/request";

export interface Props {
  custommer: Custommer;
  setCustommer: Function;
}

const CustomerInfomation = (props: Props) => {
  const { productItemCart, setProductItemCart }: any = useContext(TextValues);
  const { custommer, setCustommer } = props;
  const [result, setResult] = useState({
    text: "",
    boolean: false,
  });

  // console.log("custommer", custommer);
  const handleOrder = async (e: any) => {
    e.preventDefault();
    for (let item of productItemCart) {
      if (
        custommer?.email !== "" &&
        custommer?.hoten !== "" &&
        custommer?.sdt !== "" &&
        custommer?.address !== ""
      ) {
        await request.post("customer", {
          ...custommer,
          idProduct: item?._id,
          count: item?.count,
        });

        setCustommer({
          email: "",
          hoten: "",
          sdt: "",
          address: "",
          ghichu: "",
        });
        setProductItemCart([]);
        setResult({ text: "đặt hàng thành công", boolean: true });
      } else {
        setResult({ text: "đặt hàng không thành công", boolean: false });
      }
    }
  };

  return (
    <div className="py-16 px-5 bg-[#f8f7f4]">
      <div className="w-[670px] px-[60px]  flex flex-col justify-center items-center">
        <Link href="/" className="mb-10">
          <Logo />
        </Link>
        <h3 className="pb-8 text-xl uppercase font-bold">
          Thông tin khách hàng
        </h3>
        <form className="w-full">
          <input
            type="text"
            placeholder="Email"
            value={custommer?.email}
            required
            onChange={(e) =>
              setCustommer({ ...custommer, email: e.target.value })
            }
            className="border-[1px] w-full h-[50px] px-4 my-4 text-base outline-none border-[rgba(85_108_147_.15)] bg-[#f8f7f4] "
          />
          <div className="flex justify-between">
            <input
              type="text"
              placeholder="Họ tên"
              value={custommer?.hoten}
              required
              onChange={(e) =>
                setCustommer({ ...custommer, hoten: e.target.value })
              }
              className="border-[1px] w-[264px] h-[50px] px-4 my-4 text-base outline-none border-[rgba(85_108_147_.15)] bg-[#f8f7f4] "
            />
            <input
              type="text"
              placeholder="Số điện thoại"
              value={custommer?.sdt}
              required
              onChange={(e) =>
                setCustommer({ ...custommer, sdt: e.target.value.trim() })
              }
              className="border-[1px] w-[264px] h-[50px] px-4 my-4 text-base outline-none border-[rgba(85_108_147_.15)] bg-[#f8f7f4] "
            />
          </div>
          <input
            type="text"
            placeholder="Địa chỉ"
            value={custommer?.address}
            required
            onChange={(e) =>
              setCustommer({
                ...custommer,
                address: e.target.value,
              })
            }
            className="border-[1px] w-full h-[50px] px-4 my-4 text-base outline-none border-[rgba(85_108_147_.15)] bg-[#f8f7f4] "
          />
          <textarea
            placeholder="Nhập ghi chú nếu cần"
            className="border-[1px] w-full h-[160px] px-4 my-4 text-base outline-none border-[rgba(85_108_147_.15)] bg-[#f8f7f4] "
            value={custommer.ghichu}
            onChange={(e) =>
              setCustommer({ ...custommer, ghichu: e.target.value })
            }
          ></textarea>
        </form>
        <div className="text-center flex flex-col justify-center items-center">
          <p className="text-base">
            *Phương thức vận chuyển là
            <span className="text-[#58c772] font-bold mx-1">FREESHIP</span> với
            đơn hàng từ 700.000đ
          </p>
          <button
            className="w-[320px] bg-[#58c772] py-5 my-10 rounded-xl text-white text-sm font-bold uppercase flex justify-center items-center hover:opacity-90"
            onClick={handleOrder}
          >
            Đặt hàng
            <BsArrowRight className="ml-2 text-xl" />
          </button>

          <label
            className={`${result.boolean ? "text-green-300" : "text-red-500"}`}
          >
            {result.text}
          </label>
        </div>
      </div>
    </div>
  );
};

export default CustomerInfomation;
