import React, { useState } from "react";
import ContentChange from "./productInfo/contentChange";
import ContentInfo from "./productInfo/contentInfo";
import ContentPay from "./productInfo/contentPay";
import ContentShip from "./productInfo/contentShip";

const Policys: React.FC = (props) => {
  const { productItem } = props;
  const [hideContents, setHideContents] = useState<String>("ContentInfo");
  return (
    <div className="pt-20 px-8 pb-4 flex  justify-center bg-[#f8f7f4]">
      <div className="w-[1170px] opacity-70">
        <div className="flex justify-between  border-b-[3px] border-[#a6a6a4]">
          <div
            className={`p-3 cursor-pointer hover:border-b-[2px] border-[#161a21] ${
              hideContents === "ContentInfo" &&
              "border-b-[2px] border-[#161a21]"
            }`}
            onClick={() => setHideContents("ContentInfo")}
          >
            <span className="py-3 px-5 uppercase text-base text-[#161a21] font-normal ">
              Thông tin sản phẩm
            </span>
          </div>
          <div
            className={`p-3 cursor-pointer hover:border-b-[2px] border-[#161a21] ${
              hideContents === "ContentShip" &&
              "border-b-[2px] border-[#161a21]"
            }`}
            onClick={() => setHideContents("ContentShip")}
          >
            <span className="py-3 px-5 uppercase text-base text-[#161a21] font-normal ">
              Chính sách vận chuyển
            </span>
          </div>
          <div
            className={`p-3 cursor-pointer hover:border-b-[2px] border-[#161a21] ${
              hideContents === "ContentChange" &&
              "border-b-[2px] border-[#161a21]"
            }`}
            onClick={() => setHideContents("ContentChange")}
          >
            <span className="py-3 px-5 uppercase text-base text-[#161a21] font-normal ">
              Đổi trả & bảo hành
            </span>
          </div>
          <div
            className={`p-3 cursor-pointer hover:border-b-[2px] border-[#161a21] ${
              hideContents === "ContentPay" && "border-b-[2px] border-[#161a21]"
            }`}
            onClick={() => setHideContents("ContentPay")}
          >
            <span className="py-3 px-5 uppercase text-base text-[#161a21] font-normal ">
              Hình thức thanh toán
            </span>
          </div>
        </div>
        {hideContents === "ContentInfo" && (
          <ContentInfo productItem={productItem} />
        )}
        {hideContents === "ContentShip" && <ContentShip />}
        {hideContents === "ContentChange" && <ContentChange />}
        {hideContents === "ContentPay" && <ContentPay />}
      </div>
    </div>
  );
};

export default Policys;
