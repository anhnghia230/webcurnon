/* eslint-disable @next/next/no-img-element */
import React, { useContext, useState } from "react";

import { BsStar } from "react-icons/bs";
import { CgSize } from "react-icons/cg";
import WristSize from "@/component/wristSize/wristSize";
import Link from "next/link";
import { TextValues } from "@/component/layout";

const ProductItem = (props: any) => {
  const { productItem } = props;
  const { plusProduct }: any = useContext(TextValues);

  const installment = Math.round(productItem?.price / 3);

  const [hideWristSize, setHideWristSie] = useState<boolean>(false);
  return (
    <div className="w-[1170px] py-14 px-5 flex justify-center gap-8 ">
      <div className="flex gap-8">
        <div className="flex flex-col justify-center items-center">
          <img
            src={productItem?.img}
            width={80}
            height={80}
            alt="product-img"
            className="bg-white rounded-full bg-cover "
          />
        </div>
        <div>
          <img
            src={productItem?.img}
            width={562}
            height={562}
            alt="product-img"
          />
        </div>
      </div>
      <div className="w-[460px] bg-[#f8f7f4] ">
        <div className="p-8 flex flex-col justify-center items-center">
          <p className="text-base text-[#161a21] opacity-60 uppercase">
            {productItem?.brand}
          </p>
          <h3 className="text-4xl text-[#161a21] uppercase pt-3 pb-6">
            {productItem?.name}
          </h3>
          <div className="text-base text-[#161a21] font-bold flex items-center">
            <span>
              {productItem?.priceSell?.toLocaleString("vi", {
                style: "currency",
                currency: "VND",
              })}
            </span>
            <span
              className={`ml-2 ${
                productItem?.priceSell ? " line-through opacity-60 text-sm" : ""
              }`}
            >
              {productItem?.price?.toLocaleString("vi", {
                style: "currency",
                currency: "VND",
              })}
            </span>
            <p className="flex items-center text-sm ml-2 opacity-50">
              {Array(5)
                .fill(0)
                .map((i, index) => (
                  <BsStar key={index} className="text-xs" />
                ))}

              <span className="ml-1 opacity-90 font-normal">{`(0)`}</span>
            </p>
          </div>
          <p className="text-base pt-4">
            <span className="mx-1 opacity-50">hoặc</span>
            <span className="opacity-70">
              {installment?.toLocaleString("vi", {
                style: "currency",
                currency: "VND",
              })}
            </span>
            <span className="mx-1 opacity-50"> x 3 kỳ với</span>
            <span className="text-[#54a6ff] mx-1">Fundiin</span>
          </p>
          <div className="flex justify-center items-center py-7">
            <p className="pr-5">
              Tình trạng : <span className="text-[#5eb53e]">Còn hàng</span>
            </p>
            <div className="flex justify-center items-center pl-5 py-1 border-l border-[#d6d6d4]">
              <CgSize />
              <span
                className="text-base opacity-70 underline ml-1 italic cursor-pointer "
                onClick={() => setHideWristSie(true)}
              >
                Cỡ cổ tay
              </span>
              {hideWristSize && <WristSize setHideWristSie={setHideWristSie} />}
            </div>
          </div>
          <Link
            href="/paypage"
            className="text-base text-white font-bold uppercase bg-[#53c66e] py-5 w-full text-center group"
            onClick={() => plusProduct(productItem)}
          >
            <span className="group-hover:opacity-70"> Thanh toán ngay</span>
          </Link>

          <button
            className="text-base text-[#161a21] font-bold uppercase border-[1px] border-[#161a21] py-5 w-full text-center group hover:bg-[#161a21] mt-4 "
            onClick={() => plusProduct(productItem)}
          >
            <span className="group-hover:text-white">Thêm vào giỏ hàng</span>
          </button>
        </div>
      </div>
    </div>
  );
};

export default ProductItem;
