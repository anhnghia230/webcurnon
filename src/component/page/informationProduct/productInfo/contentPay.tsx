import Image from "next/image";
import React from "react";
import Images from "../../../../../public/image/image";

const ContentPay: React.FC = () => {
  return (
    <div className="py-5 pl-4">
      <div>
        <p className="my-3">Curnon chấp nhận các hình thức thanh toán sau:</p>
        <p className="my-3 font-bold">
          Trả tiền mặt khi nhận hàng, Ví điện tử Momo, Ví điện tử VNPay, Trả góp
          theo kỳ hạn qua Fundiin
        </p>
        <div className="flex gap-2 py-4">
          <Image src={Images.momo} width={38} height={38} alt="momo" />
          <Image src={Images.vnpay} width={38} height={38} alt="vnpay" />
        </div>
        <p className="my-3 font-bold">
          Hoặc chuyển khoản ngân hàng qua tài khoản:
        </p>
        <div className="my-3">
          <p>Số tài khoản: 21510002970496</p>
          <p>
            Chủ tài khoản: NH thương mại cổ phần đầu tư và phát triển Việt Nam
          </p>
          <p>Tên ngân hàng:BIDV</p>
          <p>Chi nhánh: Hà Nội</p>
        </div>
      </div>
    </div>
  );
};

export default ContentPay;
