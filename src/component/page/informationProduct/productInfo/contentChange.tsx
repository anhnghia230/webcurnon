import React from "react";

const ContentChange: React.FC = () => {
  return (
    <div className="py-5 pl-4 text-base">
      <div>
        <p className="py-4">Chính sách đổi trả:</p>
        <p className="py-1">
          - <span className="font-bold">1 ĐỔI 1</span>
          {` trong vòng 3 ngày kể từ khi nhận hàng (kèm theo
          các điều kiện)`}
        </p>
      </div>
      <div>
        <p className="py-4">Chính sách bảo hành:</p>
        <p className="py-1">
          - <span className="font-bold uppercase">Bảo hành 10 năm</span> đối với
          những lỗi từ nhà sản xuất
        </p>
        <p className="py-1">
          -
          <span className="font-bold">
            {` BẢO HÀNH MIỄN PHÍ (1 lần) trong 1 năm đầu tiên`}
          </span>
          với những lỗi người dùng như: vỡ, nứt kính, hấp hơi nước, va đập mạnh,
          rơi linh kiện bên trong mặt đồng hồ...
        </p>
        <p className="py-1 font-bold">- THAY PIN MIỄN PHÍ TRỌN ĐỜI</p>
      </div>
    </div>
  );
};

export default ContentChange;
