import React from "react";

const ContentShip = () => {
  return (
    <div className="py-5 pl-4 text-base">
      <div>
        <p className="my-4">Phí vận chuyển:</p>
        <p className="p-1">
          -
          <span className="font-bold uppercase mr-1"> Miễn phí vận chuyển</span>
          với đơn hàng từ 700,000đ trở lên
        </p>
        <p className="p-1">
          - <span className="font-bold uppercase mr-1">30,000đ</span> với đơn
          hàng có giá trị thấp hơn 700,000đ
        </p>
      </div>
      <div>
        <p className="my-4">Thời gian vận chuyển:</p>
        <p className="p-1">- Nội thành Hà Nội: 1-2 ngày</p>
        <p className="p-1">- Miền Trung: 3-5 ngày</p>
        <p className="p-1">- Miền Nam: 5-7 ngày</p>
      </div>
    </div>
  );
};

export default ContentShip;
