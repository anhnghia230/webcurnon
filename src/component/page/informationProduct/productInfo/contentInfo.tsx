import React from "react";

const ContentInfo: React.FC = (props) => {
  const { productItem } = props;
  return (
    <div className="pt-5 flex gap-5">
      <div className="w-[570px]">
        <p className="text-base opacity-70">{productItem?.desc}</p>
      </div>
      <div className="w-[570px]">
        <div className="text-base text-[#161a21] flex justify-between items-center border-b border-[#d6d6d4] opacity-70">
          <p className="my-4 ">Kích thước mặt</p>
          <p className="my-4 uppercase ">{productItem?.size}</p>
        </div>
        <div className="text-base text-[#161a21] flex justify-between items-center border-b border-[#d6d6d4] opacity-70">
          <p className="my-4 ">Độ dày</p>
          <p className="my-4 uppercase ">{productItem?.doday}</p>
        </div>
        <div className="text-base text-[#161a21] flex justify-between items-center border-b border-[#d6d6d4] opacity-70">
          <p className="my-4 ">Màu mặt</p>
          <p className="my-4 uppercase ">{productItem?.maumat}</p>
        </div>
        <div className="text-base text-[#161a21] flex justify-between items-center border-b border-[#d6d6d4] opacity-70">
          <p className="my-4 ">Loại máy</p>
          <p className="my-4 uppercase ">{productItem?.loaimay}</p>
        </div>
        <div className="text-base text-[#161a21] flex justify-between items-center border-b border-[#d6d6d4] opacity-70">
          <p className="my-4 ">Kích cỡ dây</p>
          <p className="my-4 uppercase ">{productItem?.sizeday}</p>
        </div>
        <div className="text-base text-[#161a21] flex justify-between items-center border-b border-[#d6d6d4] opacity-70">
          <p className="my-4 ">Chống nước</p>
          <p className="my-4 uppercase ">{productItem?.chongnuoc}</p>
        </div>
        <div className="text-base text-[#161a21] flex justify-between items-center border-b border-[#d6d6d4] opacity-70">
          <p className="my-4 ">Mặt kính</p>
          <p className="my-4 uppercase ">{productItem?.matkinh}</p>
        </div>
        <div className="text-base text-[#161a21] flex justify-between items-center  opacity-70">
          <p className="my-4 ">Chất liệu dây</p>
          <p className="my-4 uppercase ">{productItem?.chatlieuday}</p>
        </div>
      </div>
    </div>
  );
};

export default ContentInfo;
