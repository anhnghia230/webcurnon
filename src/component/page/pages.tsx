/* eslint-disable @next/next/no-img-element */
import Link from "next/link";
import React from "react";

import { Data } from "@/interface/interface";

const Pages: React.FC<Data> = (props) => {
  const { item } = props;

  return (
    <>
      <Link
        key={item?._id}
        href={`/pageBrands/${item?._id}`}
        className="flex flex-col justify-center items-center hover:-translate-y-1 w-32 h-32 cursor-pointer"
      >
        <div className=" rounded-full bg-[#ecebea]">
          <img
            className="p-2"
            src={item?.img}
            alt="image watch"
            width={100}
            height={100}
          />
        </div>
        <div>
          <span className="text-[#717376] uppercase text-sm">
            {item?.brand}
          </span>
        </div>
      </Link>
    </>
  );
};

export default Pages;
