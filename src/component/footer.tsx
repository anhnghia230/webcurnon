import React from "react";
import Image from "next/image";

import { FiFacebook } from "react-icons/fi";
import { GrInstagram } from "react-icons/gr";
import { AiFillYoutube } from "react-icons/ai";
import Images from "../../public/image/image";

const Footer = () => {
  return (
    <footer className=" flex justify-around px-4 pt-16 text-sm bg-[#f8f7f4] border-t border-gray-300">
      <div className="text-sm w-96">
        <h3 className="pb-8 uppercase">Nhận thông tin mới nhất từ curnon</h3>
        <div className="flex justify-between mb-3">
          <select
            name="sex"
            id="1"
            className="p-3 outline-none border border-[#d6d6d4] bg-[#f8f7f4]  "
          >
            <option value="Giới tính">Giới tính</option>
            <option value="Nam">Nam</option>
            <option value="Nữ">Nữ</option>
          </select>
          <input
            type="text"
            placeholder="Họ tên..."
            className=" border border-[#d6d6d4] outline-none p-3 w-full ml-2 bg-[#f8f7f4]"
          />
        </div>
        <div className="w-full">
          <input
            type="text"
            placeholder="Email..."
            className="border border-[#d6d6d4] outline-none p-3 w-full mb-3 bg-[#f8f7f4] "
          />
        </div>
        <button className="uppercase mt-1 outline-none py-3 bg-[#87888a] text-[#f8f7f4] w-full ">
          <span className="hover:opacity-60 font-bold"> Đăng ký ngay</span>
        </button>
      </div>
      <div>
        <div className="pb-8">
          <h3 className="uppercase"> Liên lạc</h3>
        </div>
        <div className="pb-5">
          <p className="opacity-80">cskh@curnonwatch.com</p>
        </div>
        <div className="pb-5">
          <p className="opacity-80">0868889103</p>
        </div>
        <div className="flex pb-6">
          <div className="w-10 h-10 rounded-full bg-[#161a21] text-center flex justify-center items-center">
            <FiFacebook className=" text-white text-xl" />
          </div>
          <div className="w-10 h-10 rounded-full bg-[#161a21] text-center flex justify-center items-center ml-2">
            <GrInstagram className=" text-white text-xl" />
          </div>
          <div className="w-10 h-10 rounded-full bg-[#161a21] text-center flex justify-center items-center ml-2">
            <AiFillYoutube className=" text-white text-xl" />
          </div>
        </div>
        <div className="flex">
          <Image src={Images.momo} alt="momo" className="w-12" />
          <Image src={Images.vnpay} alt="vnpay" className="w-12 ml-2" />
        </div>
      </div>
      <div>
        <div className="pb-10">
          <h3 className="uppercase pb-8">Hanoi stores</h3>
          <p className="pb-6 opacity-70">33 Hàm Long, Hoàn Kiếm.</p>
          <p className="pb-6 opacity-70">9 B7 Phạm Ngọc Thạch, Đống Đa.</p>
          <p className="pb-6 opacity-70">173C Kim Mã, Ba Đình.</p>
          <p className="opacity-70">82 Cầu Giấy, Quan Hoa, Cầu Giấy.</p>
        </div>
        <div className="pb-10">
          <h3 className="uppercase pb-8">TP.HCM stores</h3>
          <p className="pb-6 opacity-70">
            25 Nguyễn Trãi, P.Bến Thành, Quận 1.
          </p>
          <p className="pb-6 opacity-70">348 Lê Văn Sỹ, Phường 14, Quận 3.</p>
          <p className="opacity-70">349 Quang Trung, Phường 10, Quận Gò Vấp.</p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
