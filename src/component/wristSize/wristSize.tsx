import Image from "next/image";
import React from "react";

import Images from "../../../public/image/image";
import styles from "../../styles/Home.module.css";
import { AiOutlineClose } from "react-icons/ai";

interface Fake {
  id: Number;
  name: String;
  size: String;
}

const sizeFake: Fake[] = [
  {
    id: 1,
    name: "Kashmir 40mm",
    size: "15,5-17,5cm",
  },
  {
    id: 2,
    name: "Weimar 40mm",
    size: "16-17,5cm",
  },
  {
    id: 3,
    name: "Jackson 40mm",
    size: "16-17,5cm",
  },
  {
    id: 4,
    name: "Detroit 40mm ",
    size: "16-18cm",
  },
  {
    id: 5,
    name: "Colosseum 42mm",
    size: "16-18cm",
  },
  {
    id: 6,
    name: "Whitesands 38mm",
    size: "14,5-17cm",
  },
  {
    id: 7,
    name: "Futura 40mm",
    size: "16-18cm",
  },
];

const WristSize: React.FC = (props) => {
  const { setHideWristSie } = props;
  return (
    <div className={`fixed top-20 left-0 right-0 bottom-0 ${styles.WristSize}`}>
      <div className="flex justify-center items-center h-full ">
        <div className="w-[750px] h-full flex items-center  ">
          <div className="p-6 w-[375px] h-[524px] gap-2 bg-[#f8f7f4]">
            <p className="text-xl uppercase font-medium">Mặt đồng hồ </p>
            <p className="text-sm pt-3 uppercase">
              Đối chiếu với chu vi cổ tay
            </p>
            <div>
              <div className="flex justify-between py-3 px-5 text-base text-[#161a21] font-bold">
                <p>Đồng hồ</p>
                <p>Cổ tay</p>
              </div>
              {sizeFake.map((item) => (
                <div
                  className="flex justify-between bg-[#f1f0ee] py-2 px-5 text-base my-3"
                  key={item?.id}
                >
                  <p>{item?.name}</p>
                  <p>{item?.size}</p>
                </div>
              ))}
            </div>
          </div>
          <div>
            <div className="relative">
              <Image
                src={Images.wristSize}
                alt="wristSize-img"
                className="w-[375px] h-[524px] "
              />
              <AiOutlineClose
                className="absolute top-0 right-0 text-4xl text-white bg-[#161a21] cursor-pointer "
                onClick={() => setHideWristSie(false)}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default WristSize;
