/* eslint-disable @next/next/no-img-element */
import React from "react";
import { useRouter } from "next/router";

import styles from "../../../styles/Home.module.css";

const MenuAccessoryWoment: React.FC = (props: any) => {
  const { item } = props;
  const router = useRouter();
  return (
    <div
      className={`flex justify-start items-center gap-8  pl-16 absolute  py-4 w-[820px] h-[350px] top-[0px] right-[170px] bg-[#f8f7f4] ${styles.menuAccessory}`}
    >
      <div>
        <img src={item?.img} alt="rope" className="w-[270px] h-[270px]" />
      </div>

      <div className="w-[250px] pl-7 tracking-wider leading-4">
        <div className="mb-8">
          <p className="text-sm text-[#9e9fa0]">{item?.text}</p>
        </div>
        <button
          className=" bg-[#161a21] py-3 px-9 text-white font-bold uppercase text-sm rounded-sm"
          onClick={() => router.push(`/pageAccessory/${item?._id}`)}
        >
          Khám phá ngay
        </button>
      </div>
    </div>
  );
};

export default MenuAccessoryWoment;
