/* eslint-disable react-hooks/rules-of-hooks */
import React, { useContext, useState } from "react";
import Link from "next/link";

import { AiOutlineShoppingCart, AiOutlineSearch } from "react-icons/ai";
import Logo from "../../../public/image/logo";
import Menu from "./menuMen/menu";
import MenuWoment from "./menuWoment/menuWoment";
import styles from "../../styles/Home.module.css";
import Search from "../search/search";
import Cart from "../cart/cart";
import { TextValues } from "../layout";

const header: React.FC = () => {
  const [hideSearch, setHideSearch] = useState<boolean>(false);
  const { productItemCart, hideCart, setHideCart }: any =
    useContext(TextValues);
  return (
    <header className="h-20 w-full flex justify-between items-center px-[100px] text-sm bg-[#f8f7f4] text-[#696b6f] border-b border-gray-300 fixed z-50">
      <ul className="grid grid-cols-3 gap-x-6 uppercase">
        <div className={styles.nam}>
          <li className="cursor-pointer">Nam giới</li>
          <Menu />
        </div>
        <div className={styles.nu}>
          <li className="cursor-pointer">Nữ giới</li>
          <MenuWoment />
        </div>
        <li className="cursor-pointer">Về curnon</li>
      </ul>
      <Link href="/" className="mr-20">
        <Logo />
      </Link>

      <ul className="grid grid-cols-2 gap-x-6 uppercase ">
        <li>
          <div
            className="flex relative "
            onClick={() => setHideCart(!hideCart)}
          >
            <span className="cursor-pointer"> Giỏ hàng</span>
            <AiOutlineShoppingCart className="ml-2 text-lg cursor-pointer" />

            <p className="absolute -right-3 -top-2 bg-red-300 text-white rounded-full w-5 h-5 text-center">
              {productItemCart.length}
            </p>
          </div>
          {hideCart && <Cart setHideCart={setHideCart} />}
        </li>
        <li className={styles.headerSearch}>
          <AiOutlineSearch
            className="text-xl cursor-pointer"
            onClick={() => setHideSearch(!hideSearch)}
          />
          {hideSearch && <Search setHideSearch={setHideSearch} />}
        </li>
      </ul>
    </header>
  );
};

export default header;
