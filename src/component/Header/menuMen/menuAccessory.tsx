/* eslint-disable @next/next/no-img-element */
import React from "react";

import { BsArrowRight } from "react-icons/bs";
import styles from "../../../styles/Home.module.css";
import Link from "next/link";

const MenuAccessory: React.FC = (props: any) => {
  const { item } = props;

  return (
    <div
      className={`flex justify-start items-center gap-8  pl-16 absolute  py-4 w-[820px] h-[350px] top-[0px] right-[170px] bg-[#f8f7f4]  ${styles.menuAccessory}`}
    >
      <Link
        href={`/pageAccessory/${item?._id}`}
        className="w-32 h-32 flex flex-col justify-center items-center  hover:-translate-y-2 cursor-pointer "
      >
        <img
          src={item?.img}
          alt="cuff"
          width={100}
          height={100}
          className="mb-2"
        />
        <span className="uppercase ">{item?.text}</span>
      </Link>

      <div className="flex flex-col justify-center items-center hover:-translate-y-1 cursor-pointer">
        <span className="mb-2 text-[#717376] uppercase text-sm">
          Xem tất cả
        </span>
        <BsArrowRight className="text-xl" />
      </div>
    </div>
  );
};

export default MenuAccessory;
