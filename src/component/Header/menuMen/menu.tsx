/* eslint-disable @next/next/no-img-element */
// import Image from "next/image";
import Link from "next/link";
import React from "react";

import { BsArrowRight } from "react-icons/bs";
import { TextValues } from "../../layout";
import MenuAccessory from "./menuAccessory";
import styles from "../../../styles/Home.module.css";
import MenuRope from "../menuRope";
import Pages from "@/component/page/pages";
import { AccessoryPage } from "@/pages/pageAccessory/[id]";
import { RopePage } from "@/pages/pageRope/[id]";

const Menu: React.FC = () => {
  const { brandData, accessoryPages, ropePages }: any =
    React.useContext(TextValues);

  return (
    <nav
      className={`p-10 bg-[#f8f7f4] flex justify-center items-center absolute ${styles.menuMen} w-full mt-8 z-20 left-0 `}
    >
      <div className="py-4 pr-16 grid gap-12 border-r border-[#cacbca]  ">
        <div>
          <Link href="/">
            <span
              className={`text-4xl uppercase font-bold text-[#9e9fa0] hover:text-[#161a21] `}
            >
              Đồng hồ
            </span>
          </Link>

          <div className="mt-2">
            <Link href="/">
              <span className="text-base uppercase text-[#9e9fa0] hover:text-[#161a21]">
                Bán chạy nhất
              </span>
            </Link>
          </div>
        </div>

        <div className={styles.phukien}>
          <Link href="/">
            <span className="text-4xl uppercase font-bold text-[#9e9fa0] hover:text-[#161a21]">
              Phụ kiện
            </span>
          </Link>
          {accessoryPages.map((item: AccessoryPage) =>
            item.genres === "nam" ? (
              <MenuAccessory key={item?._id} item={item} />
            ) : (
              ""
            )
          )}
        </div>
        <div className={styles.daydh}>
          <Link href="/">
            <span className="text-4xl uppercase font-bold text-[#9e9fa0] hover:text-[#161a21]">
              Dây đồng hồ
            </span>
          </Link>
          {ropePages.map((item: RopePage) =>
            item.genres === "nam" ? (
              <MenuRope key={item?._id} item={item} />
            ) : (
              ""
            )
          )}
        </div>
      </div>
      <div className={`pl-16 py-0.5  `}>
        <div className="grid grid-cols-5 gap-5">
          {brandData.map((item: AccessoryPage) =>
            item.genres === "nam" ? <Pages item={item} key={item?._id} /> : ""
          )}
          <div className="flex flex-col justify-center items-center hover:-translate-y-1 cursor-pointer">
            <span className="mb-2 text-[#717376] uppercase text-sm">
              Xem tất cả
            </span>
            <BsArrowRight className="text-xl" />
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Menu;
