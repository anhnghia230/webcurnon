import React from "react";
import { useState, useEffect } from "react";
import Image from "next/image";

import styles from "../../styles/Home.module.css";
import { BsX } from "react-icons/bs";
import request from "@/axios/request";
import ListProduct from "../listProduct/listProduct";
import Images from "../../../public/image/image";
import Debounce from "@/hook/Debounce";

interface ListSearch {
  brand: string;
}

const listSearch = [
  "kashmir",
  "colosseum",
  "whitesands",
  "moraine",
  "hamilton",
];

const Search: React.FC<ListSearch[]> = (props) => {
  const { setHideSearch } = props;
  const [searchResult, setSearchResult] = useState("");
  const [productResult, setProductResult] = useState([]);

  const debounce = Debounce(searchResult, 1000);

  useEffect(() => {
    const fetchData = async () => {
      const data = await request.get("product");
      const result = await data.data.filter(
        (item) => item.brand === searchResult
      );
      setProductResult(result);
    };
    fetchData();
  }, [searchResult, debounce]);

  const handleOnchange = (e: any) => {
    setSearchResult(e.target.value);
  };

  return (
    <div
      className={` w-full bg-[#f8f7f4] flex justify-center border-b border-[#d1d5db]  z-10 ${styles.search} `}
    >
      <div className="w-[1170px]  p-6 flex flex-col  gap-10 ">
        <div className="flex items-center justify-end">
          <div>
            <input
              type="text"
              value={searchResult}
              onChange={handleOnchange}
              placeholder="search..."
              className="w-[570px] h-9 px-0.5 py-0.5 outline-none border-b border-[#161a21] bg-[#f8f7f4] text-xl relative"
            />
            {searchResult && (
              <BsX
                className="text-2xl absolute top-[34px] left-[1030px] cursor-pointer "
                onClick={() => setSearchResult("")}
              />
            )}
          </div>
          <button
            className="p-2 pb-0 uppercase border-b border-[#161a21] ml-52"
            onClick={() => setHideSearch(false)}
          >
            Đóng
          </button>
        </div>
        {searchResult ? (
          <ListProduct products={productResult} />
        ) : (
          <div className="flex flex-col justify-center items-center">
            <p className="pb-6">Các từ khóa nổi bật</p>
            {listSearch.map((item, index) => (
              <p
                key={index}
                className="pb-4 cursor-pointer"
                onClick={() => setSearchResult(item)}
              >
                {item}
              </p>
            ))}
          </div>
        )}
        {searchResult && productResult.length === 0 && (
          <div className="flex flex-col justify-center items-center">
            <p>Rất tiếc chúng tôi không có sản phẩm bạn cần tìm </p>
            <Image src={Images.buon} alt="buon" width={270} height={270} />
          </div>
        )}
      </div>
    </div>
  );
};

export default Search;
