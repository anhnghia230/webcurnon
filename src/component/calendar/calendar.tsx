import React, { useState } from "react";
import dayjs from "dayjs";

import { AiOutlineCaretRight, AiOutlineCaretLeft } from "react-icons/ai";
import { range } from "d3";
import style from "../../styles/Admin.module.css";

const Calendar: React.FC = () => {
  const [dayjsObj, setDayjsObj] = useState(dayjs());

  const month = dayjsObj.month();
  const year = dayjsObj.year();
  const daysInMonth = dayjsObj.daysInMonth();
  const dayInWeek = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  const today = dayjs();

  const handlePrev = () => {
    setDayjsObj(dayjsObj.subtract(1, "month"));
  };

  const handleNext = () => {
    setDayjsObj(dayjsObj.add(1, "month"));
  };

  return (
    <div
      className={`absolute top-10 w-64 border border-[#d5d4d3] rounded ${style.calendar}`}
    >
      <div className="flex justify-center items-center bg-green-300 p-2">
        <AiOutlineCaretLeft
          className="text-2xl cursor-pointer"
          onClick={handlePrev}
        />
        <p className="mx-10">{dayjsObj.format("DD-MM-YYYY")}</p>
        <AiOutlineCaretRight
          className="text-2xl cursor-pointer"
          onClick={handleNext}
        />
      </div>
      <div className="flex justify-between items-center m-2">
        {dayInWeek.map((item, index) => (
          <p key={index} className="gap-1  grid grid-cols-7">
            {item}
          </p>
        ))}
      </div>
      <div className="grid gap-1 grid-cols-7 text-center m-2">
        {range(daysInMonth).map((i) => (
          <p
            key={i}
            className={`rounded-full w-7 h-7 flex justify-center items-center font-bold cursor-pointer hover:bg-slate-300 ${
              i + 1 === today.date() &&
              month === today.month() &&
              year === today.year()
                ? "bg-slate-300"
                : ""
            }`}
          >
            {i + 1}
          </p>
        ))}
      </div>
    </div>
  );
};

export default Calendar;
