import React, { useEffect, useState, useMemo } from "react";

import Footer from "./footer";
import Header from "./Header/header";
import request from "@/axios/request";
import { ValuesProduct } from "@/pages/Admin/addProduct";

interface LayoutComponent {
  children: React.ReactNode;
  loading: boolean;
}
export const TextValues = React.createContext({});

const Layout: React.FC<LayoutComponent> = ({ children }) => {
  const [loading, setLoading] = useState<boolean>(true);
  const [brandData, setBrandData] = useState([]);
  const [products, setProducts] = useState([]);
  const [categorys, setCategorys] = useState([]);
  const [sliders, setSliders] = useState([]);
  const [accessoryPages, setAccessoryPages] = useState([]);
  const [ropePages, setRopePages] = useState([]);
  const [hideCart, setHideCart] = useState<boolean>(false);

  const [productItemCart, setProductItemCart] = useState<ValuesProduct[]>(
    () => {
      if (typeof window !== "undefined") {
        const get = localStorage.getItem("productItemCart");
        const getlocal = JSON.parse(get as any);
        return getlocal;
      } else {
        return [];
      }
    }
  );

  useEffect(() => {
    const jsonItemCarts = JSON.stringify(productItemCart);
    localStorage.setItem("productItemCart", jsonItemCarts);
  }, [productItemCart]);

  useEffect(() => {
    const fetchData = async () => {
      Promise.resolve(setLoading(true));

      const data = await request.get("brand");
      const productItems = await request.get("product");
      const categoryItems = await request.get("category");
      const sliderItems = await request.get("slider");
      const accessoryPageItems = await request.get("accessoryPage");
      const ropePageItems = await request.get("ropePage");

      setBrandData(data.data);
      setProducts(productItems.data);
      setCategorys(categoryItems.data);
      setSliders(sliderItems.data);
      setAccessoryPages(accessoryPageItems.data);
      setRopePages(ropePageItems.data);

      Promise.resolve(setLoading(false));
    };
    fetchData();
  }, []);

  //handle product

  const plusProduct = (productItem: ValuesProduct) => {
    const existProduct = productItemCart.find(
      (productItemCart) => productItemCart._id === productItem._id
    );
    if (existProduct) {
      setProductItemCart(
        productItemCart.map((productItemCart) =>
          productItemCart._id === productItem._id
            ? { ...existProduct, count: existProduct.count + 1 }
            : productItemCart
        )
      );
      setHideCart(true);
    } else {
      setProductItemCart([...productItemCart, { ...productItem, count: 1 }]);
      setHideCart(true);
    }
  };

  const dashProduct = (productItem: { _id: any }) => {
    const existProduct = productItemCart.find(
      (productItemCart) => productItemCart._id === productItem._id
    );
    if (existProduct.count > 1) {
      setProductItemCart(
        productItemCart.map((productItemCart) =>
          productItemCart._id === productItem._id
            ? { ...existProduct, count: existProduct.count - 1 }
            : productItemCart
        )
      );
    }
  };

  const deleteCart = (productItem: { _id: any }) => {
    const deleteCart = productItemCart.filter(
      (productItemCart) => productItemCart._id !== productItem._id
    );
    setProductItemCart(deleteCart);
  };

  const totalPrice = productItemCart.reduce(
    (price, item) => price + item.price * item.count,
    0
  );

  const values = {
    brandData,
    products,
    categorys,
    sliders,
    loading,
    productItemCart,
    setProductItemCart,
    hideCart,
    setHideCart,
    plusProduct,
    dashProduct,
    deleteCart,
    totalPrice,
    accessoryPages,
    ropePages,
  };
  return (
    <TextValues.Provider value={values}>
      <div suppressHydrationWarning>
        <Header />
        {children}
        <Footer />
      </div>
    </TextValues.Provider>
  );
};

export default Layout;
