/* eslint-disable @next/next/no-img-element */
import React, { useContext } from "react";

import styles from "../../styles/Home.module.css";
import { BsXLg, BsArrowRight, BsTruck, BsPlus, BsDash } from "react-icons/bs";
import Link from "next/link";
import { TextValues } from "../layout";
import { Data } from "@/interface/interface";

const Cart: React.FC = (props) => {
  const { setHideCart }: boolean = props;
  const {
    productItemCart,
    plusProduct,
    dashProduct,
    deleteCart,
    totalPrice,
  }: any = useContext(TextValues);

  const date = new Date();
  const day = date.getDate();
  const month = date.getMonth();
  const year = date.getFullYear();
  const dmy = day + "/" + (month + 1) + "/" + year;

  return (
    <div
      className={`fixed w-full h-full z-20 left-0 right-0 bottom-0 top-20 ${styles.cart}`}
    >
      <div className="absolute w-[400px] h-full bg-white right-0">
        <div className="w-full h-20 bg-[#161a21] flex justify-between px-5 items-center">
          <span className="text-xl text-white uppercase font-bold">
            Giỏ hàng của bạn
          </span>
          <BsXLg
            className="text-white text-xl cursor-pointer"
            onClick={() => setHideCart(false)}
          />
        </div>
        <div className="w-full h-10 flex items-center justify-center gap-2 bg-[#ecebea] ">
          <BsTruck className="text-xl" />
          <span className="text-xs">{`Miễn phí vận chuyển đơn hàng > 700K`}</span>
        </div>
        {productItemCart.length > 0 ? (
          <div>
            <div className="py-6 h-[48vh] overflow-y-auto  ">
              <div className="grid gap-6 ">
                {productItemCart.map((item: Data) => (
                  <div
                    className="px-7   flex gap-10 h-[74px]  "
                    key={item?._id}
                  >
                    <div className="relative">
                      <img
                        src={item?.img}
                        alt="product"
                        width={102}
                        height={74}
                      />
                      <BsXLg
                        className="text-black absolute top-0  text-xl bg-[#ecebea] cursor-pointer p-1"
                        onClick={() => deleteCart(item)}
                      />
                    </div>
                    <div className="w-full">
                      <div className="flex justify-between w-full">
                        <h3 className="text-sm">{item?.name}</h3>
                        <div>
                          <p className={`text-sm`}>
                            {item?.price?.toLocaleString("vi", {
                              style: "currency",
                              currency: "VND",
                            })}
                          </p>
                          <p className="line-through  pt-2  text-[#53565b] text-xs opacity-70">
                            {item?.priceSell?.toLocaleString("vi", {
                              style: "currency",
                              currency: "VND",
                            })}
                          </p>
                        </div>
                      </div>
                      <div className="flex justify-between w-full pt-3">
                        <p>40mm</p>
                        <div className="flex justify-center items-center border-l border-[#ecebea] pl-2">
                          <BsDash
                            className="pt-[1px] px-[1px] text-xl rounded-full bg-[#ecebea] cursor-pointer"
                            onClick={() => dashProduct(item)}
                          />
                          <span className="pt-0.5 px-2.5">
                            Qty:{item?.count}
                          </span>
                          <BsPlus
                            className="pt-[1px] px-[1px] text-xl rounded-full bg-[#ecebea] cursor-pointer"
                            onClick={() => plusProduct(item)}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
            <div className="w-full h-[190px] border-t border-[#ecebea]   bg-[#f8f7f4] ">
              <div className="px-10 pt-6 pb-7 flex flex-col    gap-5">
                <div className="text-sm flex justify-between">
                  <span>Thành tiền:</span>
                  <span className="text-[#ff5e57] font-bold">
                    {totalPrice?.toLocaleString("vi", {
                      style: "currency",
                      currency: "VND",
                    })}
                  </span>
                </div>

                <Link
                  href="/paypage"
                  className="flex justify-center items-center bg-[#53c66e] text-white font-bold w-full group "
                >
                  <span className="p-4 group-hover:opacity-70"> Mua hàng </span>
                  <BsArrowRight className="group-hover:opacity-70" />
                </Link>
                <div className="text-center">
                  <span className="">*Ước tính thời gian ship:</span>
                  <span className="text-[#53c66e]"> {dmy}</span>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="py-7">
            <div className="py-40 px-7 flex flex-col justify-center items-center gap-5">
              <p className="text-center normal-case text-base">
                Hiện đang chưa có sản phẩm nào trong giỏ hàng của bạn.
              </p>
              <Link
                href="/"
                className="flex justify-center items-center bg-[#161a21] text-white font-bold w-full group "
                onClick={() => setHideCart(false)}
              >
                <span className="p-4 group-hover:opacity-70"> Mua hàng </span>
                <BsArrowRight className="group-hover:opacity-70" />
              </Link>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Cart;
