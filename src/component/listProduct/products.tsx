/* eslint-disable @next/next/no-img-element */
import Link from "next/link";
import React, { useContext } from "react";

import styles from "../../styles/Home.module.css";
import { Data } from "@/interface/interface";
import { TextValues } from "../layout";

const Products: React.FC = (props) => {
  const { products } = props;

  const { plusProduct }: any = useContext(TextValues);

  return (
    <>
      {products &&
        products.map((item: Data) => (
          <div
            key={item?._id}
            className={`flex flex-col w-[270px]  items-center bg-[#f8f7f4]  ${styles.product} `}
          >
            <div className="relative">
              <Link href={`/pageProduct/${item?._id}`}>
                <img src={item?.img} alt="product" width={270} height={270} />
                {item?.bestSeller && (
                  <span className="absolute top-0 bg-[#ff5e57] text-white py-1.5 px-3 ml-3 mt-3 rounded-[100px] text-sm font-bold">
                    {item?.bestSeller}
                  </span>
                )}
              </Link>
              <div className="pb-5">
                <button
                  className={`w-[270px] h-12 py-0.5 px-1.5 bg-[#ecebea]  text-black text-sm font-semibold  uppercase absolute bottom-5 border-2 border-white ${styles.productBtn}`}
                  onClick={() => plusProduct(item)}
                >
                  Thêm vào giỏ
                </button>
              </div>
            </div>

            <Link href="/" className="mt-1">
              <span className="text-xs uppercase text-[#707376] font-medium opacity-70">
                {item?.brand}
              </span>
            </Link>
            <Link href="/" className="mt-1">
              <span className="text-sm uppercase text-[#707376] font-medium opacity-70">
                {item?.name}
              </span>
            </Link>
            <div className="py-2 text-[#161a21] font-bold">
              <span className={`text-[#161a21] `}>
                {item?.price?.toLocaleString("vi", {
                  style: "currency",
                  currency: "VND",
                })}
              </span>
              <span className="line-through text-[#707376] ml-2">
                {item?.priceSell?.toLocaleString("vi", {
                  style: "currency",
                  currency: "VND",
                })}
              </span>
            </div>
          </div>
        ))}
    </>
  );
};

export default Products;
