import React from "react";

import Skeleton from "@/Skeleton/Skeleton";

const ProductSkeleton = (props: any) => {
  const { products } = props;
  return Array(products.length)
    .fill(0)
    .map((item, index) => (
      <Skeleton key={index} className="w-[270px] h-[386px]"></Skeleton>
    ));
};

export default ProductSkeleton;
