import React, { useContext } from "react";

import { TextValues } from "../layout";
import Products from "./products";
import ProductSkeleton from "./productSkeleton";
import { Data } from "@/interface/interface";

interface ListProduct {
  products: Data;
  loading: boolean;
}

const ListProduct: React.FC<ListProduct> = (props) => {
  const { products } = props;
  const { loading } = useContext(TextValues);

  return (
    <>
      <div className=" flex justify-center items-center pt-20">
        <div className="grid grid-cols-4 gap-5 max-w-[1170px] ">
          {loading ? (
            <ProductSkeleton products={products} />
          ) : (
            <Products products={products} />
          )}
        </div>
      </div>
    </>
  );
};

export default ListProduct;
