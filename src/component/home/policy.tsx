import React from "react";

import { BsTruck, BsShieldCheck, BsRepeat } from "react-icons/bs";

const Policy: React.FC = () => {
  return (
    <div className="w-full h-16 flex justify-around items-center text-xs bg-[#ecebea] uppercase mt-0.5">
      <div className="flex items-center">
        <BsTruck className="mr-2 text-xl " />
        <span>{`Freeshhip đơn hàng >700K

`}</span>
      </div>
      <div className="flex items-center">
        <BsShieldCheck className="mr-2 text-xl" />
        <span>Bảo hành 10 năm</span>
      </div>
      <div className="flex items-center">
        <BsRepeat className="mr-2 text-xl" />
        <span>Đổi trả miễn phí trong vòng 3 ngày</span>
      </div>
    </div>
  );
};

export default Policy;
