import React from "react";

import { Data } from "@/interface/interface";
import { FaPlay } from "react-icons/fa";

const BtnSlider = (props: { item: Data }) => {
  const { brand, id, setSliderId, colorSlider } = props;

  return (
    <>
      <div className="w-[390px] h-[146px] flex flex-col">
        <div
          className={`h-0.5 w-full bg-white  ${
            colorSlider === "white" ? "bg-[#161a21]" : "bg-white"
          } `}
        ></div>
        <div
          className="my-12 flex justify-center items-center w-full cursor-pointer"
          onClick={() => setSliderId(id)}
        >
          <button
            className={`w-10 h-10 border-[1px] border-white rounded-full flex justify-center items-center mr-5 ${
              colorSlider === "white"
                ? " text-black border-[1px] border-[#161a21]"
                : "text-white border-[1px] border-white"
            }`}
          >
            <FaPlay className="  text-xl text-white " />
          </button>
          <span
            className={`text-base uppercase text-white ${
              colorSlider === "white" ? " text-black" : "text-white "
            }`}
          >
            {brand} collection
          </span>
        </div>
      </div>
    </>
  );
};

export default BtnSlider;
