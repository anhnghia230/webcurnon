/* eslint-disable @next/next/no-img-element */
import Link from "next/link";
import React, { useEffect } from "react";
import { useContext, useState } from "react";

import BtnSlider from "./btnSlider";
import { TextValues } from "@/component/layout";
import { Data } from "@/interface/interface";
import styles from "../../../styles/Home.module.css";

const Slider: React.FC = () => {
  const { sliders } = useContext(TextValues);

  const [sliderItem, setSliderItem] = useState([]);
  const [sliderId, setSliderId] = useState();

  useEffect(() => {
    const handle = async () => {
      for (let i = 0; i < sliders.length; i++) {
        setSliderItem([sliders[0]]);
      }

      const slider = await sliders.filter(
        (item: Data) => item._id === sliderId
      );
      if (slider.length > 0) {
        setSliderItem(slider);
      }
    };
    handle();
  }, [sliders, sliderId]);

  return (
    <div className="pt-40 ">
      {sliderItem.map((item) => (
        <div className="relative h-[600px] " key={item?._id}>
          <img
            src={item?.img}
            alt="slider"
            className={`w-full h-full  ${styles.slider}`}
          />
          <div className="absolute top-0 w-full h-full flex flex-col   items-center  justify-center  ">
            <div className="w-[1170px] h-full flex flex-col justify-center">
              <p
                className={`text-sm mb-3.5 uppercase font-semibold ${
                  item?.colorSlider === "white" ? "text-black" : "text-white"
                }`}
              >
                {item?.campaign}
              </p>
              <div
                className={`text-3xl pb-4 uppercase font-semibold ${
                  item?.colorSlider === "white" ? "text-black" : "text-white"
                }`}
              >
                {item?.title}
              </div>
              <div
                className={`text-xl ${
                  item?.colorSlider === "white" ? "text-black" : "text-white"
                }`}
              >
                {item?.desc}
              </div>
              <div className="mt-8">
                <Link
                  href="/"
                  className={`text-base  py-4 px-9 border-[1px] border-[#161a21] uppercase font-bold hover:bg-[#161a21] hover:text-white ${
                    item?.colorSlider === "white"
                      ? "text-black border-[1px] border-[#161a21] "
                      : "text-white border-[1px] border-white "
                  }`}
                >
                  Khám phá ngay
                </Link>
              </div>
            </div>

            <div className="flex justify-around">
              {sliders.map((item: Data) => (
                <div key={item?._id}>
                  <BtnSlider
                    brand={item?.brand}
                    id={item?._id}
                    colorSlider={item?.colorSlider}
                    setSliderId={setSliderId}
                  />
                </div>
              ))}
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Slider;
