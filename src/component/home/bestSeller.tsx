import Link from "next/link";
import React from "react";

import { BsArrowRight } from "react-icons/bs";

const BestSeller: React.FC = ({ title }) => {
  return (
    <div className="mt-24 flex flex-col justify-center items-center">
      <div className="text-3xl text-[#161a21] pb-7 uppercase">{title}</div>
      <Link
        href="/"
        className="flex justify-center items-center text-sm uppercase mb-16"
      >
        <span className="mr-2">Xem tất cả</span>
        <BsArrowRight className="text-sm" />
      </Link>
    </div>
  );
};

export default BestSeller;
