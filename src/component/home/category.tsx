/* eslint-disable @next/next/no-img-element */
import React from "react";
import Link from "next/link";
import { useContext } from "react";

import { BsArrowRight } from "react-icons/bs";
import { TextValues } from "../layout";
import { Data } from "@/interface/interface";

const Category: React.FC = () => {
  const { categorys } = useContext(TextValues);

  return (
    <div className="py-8 px-4 grid grid-cols-3 w-[1170px] m-[0_auto] ">
      {categorys.map((item: Data) => (
        <Link
          key={item?._id}
          href="/"
          className="relative w-[380px] h-[290px] group"
        >
          <img
            src={item?.img}
            alt="category"
            width={380}
            height={290}
            className="p-6  h-[290px]  "
          />
          <div className="absolute bottom-0 flex justify-between items-center w-full p-12 ">
            <span className="text-xl text-white uppercase font-medium">
              {item?.text}
            </span>
            <span className="w-8 h-8 rounded-full border-[1px] border-white flex justify-center items-center text-white group-hover:bg-white group-hover:text-black">
              <BsArrowRight />
            </span>
          </div>
        </Link>
      ))}
    </div>
  );
};

export default Category;
