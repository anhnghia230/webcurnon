import React from "react";
import { useContext, useState, useEffect } from "react";

import ListProduct from "../listProduct/listProduct";
import BestSeller from "./bestSeller";
import Category from "./category";
import Policy from "./policy";
import SliderHeader from "./sliderHeader";
import { TextValues } from "../layout";
import Slider from "./slider/slider";

const Home: React.FC = () => {
  const { products } = useContext(TextValues);
  const [bestSellerMen, setBestSellerMen] = useState([]);
  const [bestSellerWoment, setBestSellerWoment] = useState([]);

  useEffect(() => {
    const men = products.filter(
      (item: { genres: string }) => item.genres === "Nam"
    );
    const woment = products.filter(
      (item: { genres: string }) => item.genres === "nu"
    );
    setBestSellerMen(men.slice(0, 4));
    setBestSellerWoment(woment.slice(0, 4));
  }, [products]);

  return (
    <div className="pt-20 pb-24 bg-[#f8f7f4]">
      {/* <ListProduct products={products} /> */}
      <SliderHeader />
      <Policy />
      <Category />
      <div>
        <BestSeller title={"Men's best sellers"} />
        <ListProduct products={bestSellerMen} />
      </div>
      <div>
        <BestSeller title={"Women's best sellers"} />
        <ListProduct products={bestSellerWoment} />
      </div>
      <Slider />
    </div>
  );
};

export default Home;
