import Image from "next/image";
import Link from "next/link";
import React from "react";

import Images from "../../../public/image/image";

const SliderHeader: React.FC = () => {
  return (
    <div className="relative w-full flex justify-center  ">
      <Image src={Images.sliderHeader} alt="slider-header w-full" />
      <div className="absolute  w-[1140px] h-full grid grid-cols-2 text-center ">
        <div className="m-[auto_0]">
          <div className="text-base text-white font-bold mb-2 uppercase ">
            Sele off 20%
          </div>
          <div className="text-[38px] text-center text-white font-bold mb-3 uppercase">{` woment's watch collection`}</div>
          <div className="text-sm text-white mb-10">
            Sự kiện SALE dành riêng cho phái đẹp
          </div>
          <Link
            href="/"
            className="text-sm text-white py-4 px-14 font-bold uppercase border-[1px] border-white  text-center hover:bg-white hover:text-black"
          >
            Shop now
          </Link>
        </div>
      </div>
    </div>
  );
};

export default SliderHeader;
