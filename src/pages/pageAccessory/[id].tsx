/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";

import request from "@/axios/request";
import Layout from "@/component/layout";
import ListProduct from "@/component/listProduct/listProduct";

export interface AccessoryPage {
  _id?: string;
  imgSlider: string;
  title: string;
  desc: string;
  genres: string;
  img: string;
  text: string;
}

export const getStaticPaths = async () => {
  const res = await request.get("accessoryPage");
  const data = res.data;
  const paths = data.map((item: AccessoryPage) => {
    return {
      params: { id: item?._id?.toString() },
    };
  });
  return { paths, fallback: false };
};

export const getStaticProps = async ({ params }: any) => {
  const res = await request.get(`accessoryPage/${params.id}`);
  const data = await res.data;

  return {
    props: {
      accessoryPages: data,
    },
  };
};

const PageAccessory = ({ accessoryPages }: any) => {
  const [accessoryProducts, setAccessoryProducts] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      const data = await request.get("accessoryProduct");
      const accessoryItems = await data.data.filter(
        (item: AccessoryPage) => item.genres === accessoryPages?.genres
      );

      setAccessoryProducts(accessoryItems);
    };
    fetchData();
  }, [accessoryPages?.genres]);

  return (
    <Layout>
      <div className="pt-20 bg-[#f8f7f4]">
        <div key={accessoryPages?._id}>
          <div className="px-7 pt-7">
            <img src={accessoryPages?.imgSlider} alt="imgPage" />
          </div>
          <div className="mx-48 mt-14">
            <div className="text-3xl text-[#161a21] font-normal uppercase">
              {accessoryPages?.title}
            </div>
            <div>
              <p className="text-lg text-[#969798] w-[600px] font-normal mt-5">
                {accessoryPages?.desc}
              </p>
            </div>
          </div>
        </div>
        <div className="pb-20">
          <ListProduct products={accessoryProducts} />
        </div>
      </div>
    </Layout>
  );
};

export default PageAccessory;
