import React, { useState } from "react";

import Layout from "@/component/layout";
import CustomerInfomation from "@/component/page/payPageContent/customerInfomation";
import Order from "@/component/page/payPageContent/order";

export interface Custommer {
  email: string;
  hoten: string;
  sdt: string;
  address: string;
  ghichu: string;
}

const Paypages: React.FC = () => {
  const [custommer, setCustommer] = useState<Custommer>({
    email: "",
    hoten: "",
    sdt: "",
    address: "",
    ghichu: "",
  });

  return (
    <Layout loading>
      <div className="pt-20 grid grid-cols-2">
        <CustomerInfomation custommer={custommer} setCustommer={setCustommer} />
        <Order />
      </div>
    </Layout>
  );
};

export default Paypages;
