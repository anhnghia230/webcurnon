import { useEffect } from "react";
import { useRouter } from "next/router";
import { MoMoPayment } from "momo-sdk";

const Payment = () => {
  const router = useRouter();

  useEffect(() => {
    const momoPayment = new MoMoPayment({
      publicKey: "YOUR_PUBLIC_KEY",
      privateKey: "YOUR_PRIVATE_KEY",
      endpoint: "MOMO_API_ENDPOINT",
      partnerCode: "YOUR_PARTNER_CODE",
    });

    const paymentRequest = {
      amount: 100000, // Số tiền cần thanh toán
      orderId: "ORDER_ID", // Mã đơn hàng
      orderInfo: "ORDER_INFO", // Thông tin đơn hàng
      returnUrl: "CALLBACK_URL", // URL để Momo callback sau khi hoàn thành thanh toán
    };

    momoPayment
      .createPayment(paymentRequest)
      .then((response) => {
        router.push(response.payUrl); // Chuyển hướng người dùng đến trang thanh toán Momo
      })
      .catch((error) => {
        console.error(error); // Xử lý lỗi (nếu có)
      });
  }, []);

  return (
    <div>
      <p>Đang chuyển hướng đến trang thanh toán...</p>
    </div>
  );
};

export default Payment;
