/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";

import request from "@/axios/request";
import Layout from "@/component/layout";
import ListProduct from "@/component/listProduct/listProduct";

export interface RopePage {
  imgSlider: string;
  title: string;
  desc: string;
  genres: string;
  img: string;
  text: string;
  _id?: string;
}

export const getStaticPaths = async () => {
  const res = await request.get("ropePage");
  const data = await res.data;

  const paths = data.map((item: RopePage) => {
    return { params: { id: item?._id?.toString() } };
  });

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps = async ({ params }: any) => {
  const res = await request.get(`ropePage/${params?.id}`);
  const data = await res.data;

  return {
    props: { ropePages: data },
  };
};

const pageRope: React.FC = ({ ropePages }: any) => {
  const [ropeProducts, setRopeProduct] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      const res = await request.get("productRope");
      const ropeProductItems = await res.data.filter(
        (item: RopePage) => item?.genres === ropePages?.genres
      );

      setRopeProduct(ropeProductItems);
    };
    fetchData();
  }, [ropePages?.genres]);

  return (
    <Layout>
      <div className="pt-20 bg-[#f8f7f4]">
        <div key={ropePages?._id}>
          <div className="px-7 pt-7">
            <img src={ropePages?.imgSlider} alt="imgPage" />
          </div>
          <div className="mx-48 mt-14">
            <div className="text-3xl text-[#161a21] font-normal uppercase">
              {ropePages?.title}
            </div>
            <div>
              <p className="text-lg text-[#969798] w-[600px] font-normal mt-5">
                {ropePages?.desc}
              </p>
            </div>
          </div>
        </div>
        <div className="pb-20">
          <ListProduct products={ropeProducts} />
        </div>
      </div>
    </Layout>
  );
};

export default pageRope;
