import Layout from "@/component/layout";
import Home from "../component/home/home";

export default function App() {
  return (
    <>
      <Layout>
        <Home />
      </Layout>
    </>
  );
}
