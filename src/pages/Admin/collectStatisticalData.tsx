import React, { useEffect, useState } from "react";

import CollectData from "@/chart/collectData";
import { FcPlanner } from "react-icons/fc";
import Calendar from "@/component/calendar/calendar";

const CollectStatisticalData: React.FC = () => {
  useEffect(() => CollectData, []);
  const [hideCalendar, setHideCalendar] = useState<boolean>(false);
  return (
    <div className="w-full h-full">
      <div className="w-[1170px] h-[670px] border border-gray-600 m-[0_auto]">
        <div className="flex justify-between items-center">
          <div className=" w-64 text-base uppercase font-bold m-5">
            Thống kê doanh thu tháng
          </div>
          <div className="w-64  m-5 relative    ">
            <FcPlanner
              className="text-3xl cursor-pointer "
              onClick={() => setHideCalendar(!hideCalendar)}
            />
            {hideCalendar && <Calendar />}
          </div>
        </div>
        <div id="collect-chart" className="p-10"></div>
      </div>
    </div>
  );
};

export default CollectStatisticalData;
