import request from "@/axios/request";
import React, { useEffect, useState } from "react";
import { Custommer } from "../paypage";
import { BsFillTrash3Fill } from "react-icons/bs";

const OrderInformation = () => {
  const [customer, setCustomer] = useState<Custommer[]>([]);
  useEffect(() => {
    const fetchData = async () => {
      const data = await request.get("customer");
      const customers = data.data;

      setCustomer(customers);
    };
    fetchData();
  }, []);

  const handleDelete = async (id) => {
    if (confirm("bạn có chắc muốn xóa khách hàng")) {
      await request.delete(`customer/${id}`);
    }
  };

  return (
    <div className=" relative flex flex-col justify-center items-center m-10  ">
      <div className="flex flex-col justify-center items-center">
        <div className="flex mb-10   ">
          <div className=" w-44  h-full text-center ">stt</div>
          <div className=" w-44  h-full text-center ">Email KH</div>
          <div className=" w-44  h-full text-center  ">Họ tên KH</div>
          <div className=" w-44  h-full text-center  ">Số điện thoại</div>
          <div className=" w-44  h-full text-center  ">Địa chỉ nhận hàng</div>
          <div className=" w-44  h-full text-center  ">
            Ghi chú của khách hàng
          </div>
          <div className=" w-44  h-full text-center  ">Id product</div>
          <div className=" w-44  h-full text-center  ">số lượng </div>
          <div className=" w-44  h-full text-center  ">handle</div>
        </div>
        {customer.map((item, index) => (
          <div className="flex mb-10   " key={item?._id}>
            <div className=" w-44  h-full text-center ">{index}</div>
            <div className=" w-44  h-full text-center ">{item?.email}</div>
            <div className=" w-44  h-full text-center  ">{item?.hoten}</div>
            <div className=" w-44  h-full text-center  ">{item?.sdt}</div>
            <div className=" w-44  h-full text-center  ">{item?.address}</div>
            <div className=" w-44  h-full text-center  ">{item?.ghichu}</div>
            <div className=" w-44  h-full text-center  ">{item?.idProduct}</div>
            <div className=" w-44  h-full text-center  ">{item?.count}</div>
            <div
              className=" w-44  h-full text-center flex justify-center cursor-pointer "
              onClick={() => handleDelete(item?._id)}
            >
              <BsFillTrash3Fill className="text-2xl" />
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default OrderInformation;
