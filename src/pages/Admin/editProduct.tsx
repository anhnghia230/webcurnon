import React, { useEffect, useState } from "react";

import request from "@/axios/request";
import { Result, ValuesProduct } from "./addProduct";

const EditProduct: React.FC = (props) => {
  const { id }: any = props;
  const [result, setResult] = useState<Result>({
    text: "",
    boolean: false,
  });

  const [editProduct, setEditProduct] = useState<ValuesProduct>({
    img: "",
    name: "",
    brand: "",
    price: 0,
    priceSell: 0,
    bestSeller: "",
    desc: "",
    size: "",
    doday: "",
    maumat: "",
    loaimay: "",
    sizeday: "",
    chongnuoc: "",
    matkinh: "",
    chatlieuday: "",
    genres: "",
  });

  useEffect(() => {
    const handle = async () => {
      const res = await request.get(`product/${id}`);
      const data = await res.data;

      setEditProduct(data);
    };
    handle();
  }, [id]);

  const handleEditProduct = async (e: any) => {
    e.preventDefault();
    if (
      (editProduct.img !== "" &&
        editProduct.name !== "" &&
        editProduct.brand !== "" &&
        editProduct.price !== 0 &&
        editProduct.priceSell !== 0 &&
        editProduct.bestSeller !== "" &&
        editProduct.desc !== "" &&
        editProduct.size !== "" &&
        editProduct.doday !== "" &&
        editProduct.maumat !== "" &&
        editProduct.loaimay !== "" &&
        editProduct.sizeday !== "" &&
        editProduct.chongnuoc !== "" &&
        editProduct.matkinh !== "" &&
        editProduct.chatlieuday !== "",
      editProduct.genres !== "")
    ) {
      await request.put(`product/${id}`, editProduct);
      setResult({ text: "edit thành công", boolean: true });
      setEditProduct({
        img: "",
        name: "",
        brand: "",
        price: 0,
        priceSell: 0,
        bestSeller: "",
        desc: "",
        size: "",
        doday: "",
        maumat: "",
        loaimay: "",
        sizeday: "",
        chongnuoc: "",
        matkinh: "",
        chatlieuday: "",
        genres: "",
      });
    } else {
      setResult({ text: "edit không thành công", boolean: false });
    }
  };

  return (
    <div className="flex justify-center items-center py-10">
      <form className="flex flex-col w-[670px]">
        <label className="py-2">Nhập URL ảnh sp: </label>
        <input
          type="text"
          name="img"
          value={editProduct?.img}
          required
          onChange={(e) =>
            setEditProduct({ ...editProduct, img: e.target.value.trim() })
          }
          placeholder="img..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập tên sp: </label>
        <input
          type="text"
          required
          name="name"
          value={editProduct?.name}
          onChange={(e) =>
            setEditProduct({ ...editProduct, name: e.target.value.trim() })
          }
          placeholder="name..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập tên thương hiệu sp: </label>
        <input
          type="text"
          required
          name="brand"
          value={editProduct?.brand}
          onChange={(e) =>
            setEditProduct({ ...editProduct, brand: e.target.value.trim() })
          }
          placeholder="brand..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập giá sp: </label>
        <input
          type="text"
          required
          name="price"
          value={editProduct?.price}
          onChange={(e) =>
            setEditProduct({
              ...editProduct,
              price: parseInt(e.target.value),
            })
          }
          placeholder="price..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập giá sell sp: </label>
        <input
          type="text"
          name="priceSell"
          value={editProduct?.priceSell}
          onChange={(e) =>
            setEditProduct({
              ...editProduct,
              priceSell: parseInt(e.target.value),
            })
          }
          placeholder="priceSell..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập % sell sp: </label>
        <input
          type="text"
          name="bestSeller"
          value={editProduct?.bestSeller}
          onChange={(e) =>
            setEditProduct({
              ...editProduct,
              bestSeller: e.target.value,
            })
          }
          placeholder="bestSeller..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập thông tin sp: </label>
        <input
          type="text"
          required
          name="desc"
          value={editProduct?.desc}
          onChange={(e) =>
            setEditProduct({
              ...editProduct,
              desc: e.target.value.trim(),
            })
          }
          placeholder="desc..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập size sp: </label>
        <input
          type="text"
          required
          name="size"
          value={editProduct?.size}
          onChange={(e) =>
            setEditProduct({
              ...editProduct,
              size: e.target.value.trim(),
            })
          }
          placeholder="size 28MM..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập độ dày sp: </label>
        <input
          type="text"
          required
          name="doday"
          value={editProduct?.doday}
          onChange={(e) =>
            setEditProduct({
              ...editProduct,
              doday: e.target.value.trim(),
            })
          }
          placeholder="độ dày 6MM..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập màu mặt sp: </label>
        <input
          type="text"
          required
          name="maumat"
          value={editProduct?.maumat}
          onChange={(e) =>
            setEditProduct({
              ...editProduct,
              maumat: e.target.value.trim(),
            })
          }
          placeholder="màu mặt white..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập loại máy sp: </label>
        <input
          type="text"
          required
          name="loaimay"
          value={editProduct?.loaimay}
          onChange={(e) =>
            setEditProduct({
              ...editProduct,
              loaimay: e.target.value.trim(),
            })
          }
          placeholder="MIYOTA QUARTZ..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập size dây sp: </label>
        <input
          type="text"
          required
          name="sizeday"
          value={editProduct?.sizeday}
          onChange={(e) =>
            setEditProduct({
              ...editProduct,
              sizeday: e.target.value.trim(),
            })
          }
          placeholder="size dây 12MM..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập chống nước sp: </label>
        <input
          type="text"
          required
          name="chongnuoc"
          value={editProduct?.chongnuoc}
          onChange={(e) =>
            setEditProduct({
              ...editProduct,
              chongnuoc: e.target.value.trim(),
            })
          }
          placeholder="chống nước 3ATM..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập mặt kính sp: </label>
        <input
          type="text"
          required
          name="matkinh"
          value={editProduct?.matkinh}
          onChange={(e) =>
            setEditProduct({
              ...editProduct,
              matkinh: e.target.value.trim(),
            })
          }
          placeholder="mặt kính SAPPHIRE..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập chất liệu dây sp: </label>
        <input
          type="text"
          required
          name="chatlieuday"
          value={editProduct?.chatlieuday}
          onChange={(e) =>
            setEditProduct({
              ...editProduct,
              chatlieuday: e.target.value.trim(),
            })
          }
          placeholder="kim loại..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">{"Nhập genres (nam/nữ) sp"}: </label>
        <input
          type="text"
          required
          name="genres"
          value={editProduct?.genres}
          onChange={(e) =>
            setEditProduct({
              ...editProduct,
              genres: e.target.value.trim(),
            })
          }
          placeholder="..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <button
          className="py-2 w-[200px] px-4 border border-[#d1d5db] my-4 rounded hover:bg-slate-400"
          onClick={handleEditProduct}
        >
          Edit Product
        </button>
        <label
          className={`${result.boolean ? "text-green-300" : "text-red-500"}`}
        >
          {result.text}
        </label>
      </form>
    </div>
  );
};

export default EditProduct;
