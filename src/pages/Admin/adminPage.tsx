import React, { useState, useEffect } from "react";

import Admin from "@/admin";
import request from "@/axios/request";

const AdminPage: React.FC = () => {
  const [admin, setAdmin] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      const data = await request.get("admin");
      const adminItems = await data.data;

      setAdmin(adminItems);
    };
    fetchData();
  }, []);

  return <Admin admin={admin} />;
};

export default AdminPage;
