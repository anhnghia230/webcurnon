import React, { useEffect, useState } from "react";

/* eslint-disable @next/next/no-img-element */
import request from "@/axios/request";
import { ValuesProduct } from "./addProduct";
import {
  BsFillTrash3Fill,
  BsFillWrenchAdjustableCircleFill,
} from "react-icons/bs";
import EditProduct from "./editProduct";
import Link from "next/link";

const InformationProduct: React.FC = () => {
  const [edit, setEdit] = useState<{ hide: boolean; id: number }>({
    hide: false,
    id: 0,
  });
  const [products, setProducts] = useState<ValuesProduct[]>([]);

  const handleDeleteProduct = async (item: ValuesProduct) => {
    if (confirm("bạn có chắc muốn xóa sản phẩm?")) {
      await request.delete(`product/${item._id}`);
    }
  };

  useEffect(() => {
    const handle = async () => {
      const res = await request.get("product");
      const data = await res.data;
      setProducts(data);
    };
    handle();
  }, []);

  return (
    <>
      {edit.hide ? (
        <EditProduct id={edit.id} />
      ) : (
        <div className=" relative flex flex-col justify-center items-center m-10   ">
          <div className="flex flex-col justify-center items-center ">
            <div className="flex mb-10   ">
              <div className=" w-32  h-full text-center ">stt</div>
              <div className=" w-32  h-full text-center  ">Img-product</div>
              <div className=" w-32  h-full text-center  ">Name</div>
              <div className=" w-32  h-full text-center  ">Brand</div>
              <div className=" w-32  h-full text-center  ">Price</div>
              <div className=" w-32  h-full text-center  ">PriceSell</div>
              <div className=" w-32  h-full text-center  ">BestSeller</div>
              <div className=" w-32  h-full text-center  ">Size</div>
              <div className=" w-32  h-full text-center  ">genres</div>
              <div className=" w-32  h-full text-center  ">handle</div>
            </div>
          </div>

          <div className="flex flex-col justify-center items-center">
            {products.map((item, index) => (
              <div
                className="flex my-2 justify-center items-center "
                key={index}
              >
                <div className="  w-32 h-full text-center  text-base ">
                  {index}
                </div>
                <div className="  w-32 h-full text-center  ">
                  <img
                    src={item?.img}
                    alt="img-product"
                    width={100}
                    height={100}
                    className="m-[0_auto]"
                  />
                </div>
                <div className="  w-32 h-full text-center  ">{item?.name}</div>
                <div className="  w-32 h-full text-center  ">{item?.brand}</div>
                <div className="  w-32 h-full text-center  ">
                  {item?.price.toLocaleString("vi", {
                    style: "currency",
                    currency: "VND",
                  })}
                </div>
                <div className="  w-32 h-full text-center  ">
                  {item?.priceSell?.toLocaleString("vi", {
                    style: "currency",
                    currency: "VND",
                  })}
                </div>
                <div className="  w-32 h-full text-center  ">
                  {item?.bestSeller}
                </div>
                <div className="  w-32 h-full text-center   ">{item?.size}</div>
                <div className="  w-32 h-full text-center  ">
                  {item?.genres}
                </div>
                <div className="  w-32 h-full text-center flex justify-center   ">
                  <div
                    className="cursor-pointer  m-1"
                    onClick={() => setEdit({ hide: true, id: item._id })}
                  >
                    <BsFillWrenchAdjustableCircleFill className="text-2xl" />
                  </div>
                  <Link
                    href="/Admin/informationProduct"
                    className="cursor-pointer  m-1 "
                    onClick={() => handleDeleteProduct(item)}
                  >
                    <BsFillTrash3Fill className="text-2xl" />
                  </Link>
                </div>
              </div>
            ))}
          </div>
        </div>
      )}
    </>
  );
};

export default InformationProduct;
