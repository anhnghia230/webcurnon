import request from "@/axios/request";
import React, { useState } from "react";

export interface ValuesProduct {
  count: number;
  img: string;
  name: string;
  brand: string;
  price: number;
  priceSell?: number;
  bestSeller?: string;
  desc: string;
  size: string;
  doday: string;
  maumat: string;
  loaimay: string;
  sizeday: string;
  chongnuoc: string;
  matkinh: string;
  chatlieuday: string;
  genres: string;
  _id?: number;
}

export interface Result {
  text: string;
  boolean: boolean;
}

const AddProduct: React.FC = () => {
  const [result, setResult] = useState<Result>({
    text: "",
    boolean: false,
  });
  const [valueProduct, setValueProduct] = useState<ValuesProduct>({
    img: "",
    name: "",
    brand: "",
    price: 0,
    priceSell: 0,
    bestSeller: "",
    desc: "",
    size: "",
    doday: "",
    maumat: "",
    loaimay: "",
    sizeday: "",
    chongnuoc: "",
    matkinh: "",
    chatlieuday: "",
    genres: "",
  });

  const handleAddProduct = async (e: any) => {
    e.preventDefault();
    if (
      (valueProduct.img !== "" &&
        valueProduct.name !== "" &&
        valueProduct.brand !== "" &&
        valueProduct.price !== 0 &&
        valueProduct.priceSell !== 0 &&
        valueProduct.bestSeller !== "" &&
        valueProduct.desc !== "" &&
        valueProduct.size !== "" &&
        valueProduct.doday !== "" &&
        valueProduct.maumat !== "" &&
        valueProduct.loaimay !== "" &&
        valueProduct.sizeday !== "" &&
        valueProduct.chongnuoc !== "" &&
        valueProduct.matkinh !== "" &&
        valueProduct.chatlieuday !== "",
      valueProduct.genres !== "")
    ) {
      await request.post("product", valueProduct);
      setResult({ text: "add thành công", boolean: true });
      setValueProduct({
        img: "",
        name: "",
        brand: "",
        price: 0,
        priceSell: 0,
        bestSeller: "",
        desc: "",
        size: "",
        doday: "",
        maumat: "",
        loaimay: "",
        sizeday: "",
        chongnuoc: "",
        matkinh: "",
        chatlieuday: "",
        genres: "",
      });
    } else {
      setResult({ text: "add không thành công", boolean: false });
    }
  };

  return (
    <div className="flex justify-center items-center py-10">
      <form className="flex flex-col w-[670px]">
        <label className="py-2">Nhập URL ảnh sp: </label>
        <input
          type="text"
          name="img"
          value={valueProduct.img}
          required
          onChange={(e) =>
            setValueProduct({ ...valueProduct, img: e.target.value.trim() })
          }
          placeholder="img..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập tên sp: </label>
        <input
          type="text"
          required
          name="name"
          value={valueProduct.name}
          onChange={(e) =>
            setValueProduct({ ...valueProduct, name: e.target.value.trim() })
          }
          placeholder="name..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập tên thương hiệu sp: </label>
        <input
          type="text"
          required
          name="brand"
          value={valueProduct.brand}
          onChange={(e) =>
            setValueProduct({ ...valueProduct, brand: e.target.value.trim() })
          }
          placeholder="brand..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập giá sp: </label>
        <input
          type="text"
          required
          name="price"
          value={valueProduct.price}
          onChange={(e) =>
            setValueProduct({
              ...valueProduct,
              price: parseInt(e.target.value),
            })
          }
          placeholder="price..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập giá sell sp: </label>
        <input
          type="text"
          name="priceSell"
          value={valueProduct.priceSell}
          onChange={(e) =>
            setValueProduct({
              ...valueProduct,
              priceSell: parseInt(e.target.value),
            })
          }
          placeholder="priceSell..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập % sell sp: </label>
        <input
          type="text"
          name="bestSeller"
          value={valueProduct.bestSeller}
          onChange={(e) =>
            setValueProduct({
              ...valueProduct,
              bestSeller: e.target.value,
            })
          }
          placeholder="bestSeller..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập thông tin sp: </label>
        <input
          type="text"
          required
          name="desc"
          value={valueProduct.desc}
          onChange={(e) =>
            setValueProduct({
              ...valueProduct,
              desc: e.target.value.trim(),
            })
          }
          placeholder="desc..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập size sp: </label>
        <input
          type="text"
          required
          name="size"
          value={valueProduct.size}
          onChange={(e) =>
            setValueProduct({
              ...valueProduct,
              size: e.target.value.trim(),
            })
          }
          placeholder="size 28MM..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập độ dày sp: </label>
        <input
          type="text"
          required
          name="doday"
          value={valueProduct.doday}
          onChange={(e) =>
            setValueProduct({
              ...valueProduct,
              doday: e.target.value.trim(),
            })
          }
          placeholder="độ dày 6MM..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập màu mặt sp: </label>
        <input
          type="text"
          required
          name="maumat"
          value={valueProduct.maumat}
          onChange={(e) =>
            setValueProduct({
              ...valueProduct,
              maumat: e.target.value.trim(),
            })
          }
          placeholder="màu mặt white..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập loại máy sp: </label>
        <input
          type="text"
          required
          name="loaimay"
          value={valueProduct.loaimay}
          onChange={(e) =>
            setValueProduct({
              ...valueProduct,
              loaimay: e.target.value.trim(),
            })
          }
          placeholder="MIYOTA QUARTZ..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập size dây sp: </label>
        <input
          type="text"
          required
          name="sizeday"
          value={valueProduct.sizeday}
          onChange={(e) =>
            setValueProduct({
              ...valueProduct,
              sizeday: e.target.value.trim(),
            })
          }
          placeholder="size dây 12MM..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập chống nước sp: </label>
        <input
          type="text"
          required
          name="chongnuoc"
          value={valueProduct.chongnuoc}
          onChange={(e) =>
            setValueProduct({
              ...valueProduct,
              chongnuoc: e.target.value.trim(),
            })
          }
          placeholder="chống nước 3ATM..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập mặt kính sp: </label>
        <input
          type="text"
          required
          name="matkinh"
          value={valueProduct.matkinh}
          onChange={(e) =>
            setValueProduct({
              ...valueProduct,
              matkinh: e.target.value.trim(),
            })
          }
          placeholder="mặt kính SAPPHIRE..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">Nhập chất liệu dây sp: </label>
        <input
          type="text"
          required
          name="chatlieuday"
          value={valueProduct.chatlieuday}
          onChange={(e) =>
            setValueProduct({
              ...valueProduct,
              chatlieuday: e.target.value.trim(),
            })
          }
          placeholder="kim loại..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <label className="py-2">{"Nhập genres (nam/nữ) sp"}: </label>
        <input
          type="text"
          required
          name="genres"
          value={valueProduct.genres}
          onChange={(e) =>
            setValueProduct({
              ...valueProduct,
              genres: e.target.value.trim(),
            })
          }
          placeholder="..."
          className="text-base py-2 px-3 outline-none border border-[#d1d5db] rounded"
        />
        <button
          className="py-2 w-[200px] px-4 border border-[#d1d5db] my-4 rounded hover:bg-slate-400"
          onClick={handleAddProduct}
        >
          Add Product
        </button>
        <label
          className={`${result.boolean ? "text-green-300" : "text-red-500"}`}
        >
          {result.text}
        </label>
      </form>
    </div>
  );
};

export default AddProduct;
