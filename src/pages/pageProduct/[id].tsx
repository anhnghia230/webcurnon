import React from "react";

import request from "@/axios/request";
import Layout from "@/component/layout";
import { Data } from "@/interface/interface";
import ProductItem from "@/component/page/informationProduct/productItem";
import Policy from "@/component/home/policy";
import Policys from "@/component/page/informationProduct/policys";

export const getStaticPaths = async () => {
  const res = await request.get("product");
  const data = await res.data;

  const paths = data.map((item) => {
    return {
      params: { id: item?._id?.toString() },
    };
  });

  return {
    paths,
    fallback: true,
  };
};

export const getStaticProps = async ({ params }: any) => {
  const res = await request.get(`product/${params.id}`);
  const data = await res.data;

  return {
    props: { productItem: data },
  };
};

const pageProduct = ({ productItem }: Data) => {
  return (
    <Layout>
      <div className="pt-20 flex justify-center items-center bg-gradient-to-b from-[#dbdfe3] to-[#f8f7f4]">
        <ProductItem productItem={productItem} />
      </div>
      <Policy />
      <Policys productItem={productItem} />
    </Layout>
  );
};

export default pageProduct;
