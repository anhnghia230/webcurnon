/* eslint-disable @next/next/no-img-element */
import React, { useState, useEffect } from "react";

import request from "@/axios/request";
import { Data } from "@/interface/interface";
import Layout from "@/component/layout";
import ListProduct from "@/component/listProduct/listProduct";

export const getStaticPaths = async () => {
  const res = await request.get("brand");
  const data = res.data;
  const paths = data.map((item: Data) => {
    return {
      params: { id: item?._id?.toString() },
    };
  });
  return { paths, fallback: false };
};

export const getStaticProps = async ({ params }: any) => {
  const res = await request.get(`brand/${params.id}`);
  const data = await res.data;

  return {
    props: {
      pageBrand: data,
    },
  };
};

const Page: React.FC = ({ pageBrand }: any) => {
  const [productPage, setProductPage] = useState("");

  const [products, setProducts] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      setProductPage(pageBrand?.brand);
      const data = await request.get("product");

      const productItems = data.data.filter(
        (item) => item.brand == productPage
      );

      setProducts(productItems);
    };
    fetchData();
  }, [productPage, pageBrand?.brand]);

  return (
    <Layout>
      <div className="pt-20 bg-[#f8f7f4]">
        <div key={pageBrand?._id}>
          <div className="px-7 pt-7">
            <img src={pageBrand?.imgSlider} alt="imgPage" />
          </div>
          <div className="mx-48 mt-14">
            <div className="text-3xl text-[#161a21] font-normal uppercase">
              {pageBrand?.brand}
            </div>
            <div>
              <p className="text-lg text-[#969798] w-[600px] font-normal mt-5">
                {pageBrand?.desc}
              </p>
            </div>
          </div>
        </div>
        <div className="pb-20">
          <ListProduct products={products} />
        </div>
      </div>
    </Layout>
  );
};

export default Page;
