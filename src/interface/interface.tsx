export interface Data {
  [x: string]: any;
  map(
    arg0: (item: { _id: import("react").Key | null | undefined }) => JSX.Element
  ): unknown;
  campaign: ReactNode;
  title: ReactNode;
  desc: ReactNode;
  bestSeller: JSX.Element;
  brand: ReactNode;
  name: ReactNode;
  priceSell: ReactNode;
  price: ReactNode;
  _id: React.Key | null | undefined;
  img: string | undefined;
  text:
    | string
    | number
    | boolean
    | React.ReactElement<any, string | React.JSXElementConstructor<any>>
    | React.ReactFragment
    | React.ReactPortal
    | null
    | undefined;
}
