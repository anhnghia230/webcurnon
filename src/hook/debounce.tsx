import React, { useEffect, useState } from "react";

const Debounce = (values: String, time: Number) => {
  const [debounce, setDebounce] = useState(values);
  useEffect(() => {
    const handle = setTimeout(() => {
      setDebounce(values);
    }, time);
    return () => clearTimeout(handle);
  });
  return debounce;
};

export default Debounce;
